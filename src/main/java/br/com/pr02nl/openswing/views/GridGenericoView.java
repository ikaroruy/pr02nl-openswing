/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pr02nl.openswing.views;

import br.com.pr02nl.business.MyDelegateCrud;
import br.com.pr02nl.model.BaseModel;
import br.com.pr02nl.openswing.config.ViewConfig;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOListResponse;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.message.send.java.GridParams;

/**
 *
 * @author Paulo Roberto
 */
public abstract class GridGenericoView extends GridGenericoAbstract {

    public abstract MyDelegateCrud getBusiness();

    public abstract ViewConfig getConfig();

    @Override
    public Response loadData(int action, int startIndex, Map filteredColumns, ArrayList currentSortedColumns, ArrayList currentSortedVersusColumns, Class valueObjectType, Map otherGridParams) {
        if (otherGridParams.containsKey("LOAD_ALL")) {
            otherGridParams.remove("LOAD_ALL");
            List findAll = getBusiness().findAll(filteredColumns, otherGridParams, null, currentSortedColumns, currentSortedVersusColumns);
            return new VOListResponse(findAll, false, findAll.size());
        }
        int count = getBusiness().count(filteredColumns, otherGridParams);
        if (action == GridParams.PREVIOUS_BLOCK_ACTION) {
            startIndex = Math.max(startIndex - getConfig().getViewBlockSize(), 0);
        } else if (action == GridParams.LAST_BLOCK_ACTION) {
            startIndex = Math.max(count - getConfig().getViewBlockSize(), 0);
        }
        List findAll = getBusiness().findAll(filteredColumns, otherGridParams,
                new int[]{startIndex, getConfig().getViewBlockSize()}, currentSortedColumns,
                currentSortedVersusColumns);
        VOListResponse voListResponse = new VOListResponse(findAll, count > startIndex + getConfig().getViewBlockSize(), findAll.size());
        voListResponse.setTotalAmountOfRows(count);
        return voListResponse;
    }

    @Override
    public Response insertRecords(int[] rowNumbers, ArrayList newValueObjects) throws Exception {
        for (Object object : newValueObjects) {
            getBusiness().insert(object);
        }
        return new VOListResponse(newValueObjects, false, newValueObjects.size());
    }

    @Override
    public Response updateRecords(int[] rowNumbers, ArrayList oldPersistentObjects, ArrayList persistentObjects) throws Exception {
        for (Object object : persistentObjects) {
            getBusiness().update(object);
        }
        return new VOListResponse(persistentObjects, false, persistentObjects.size());
    }

    @Override
    public Response deleteRecords(ArrayList persistentObjects) throws Exception {
        for (Object object : persistentObjects) {
            BaseModel entity = (BaseModel) object;
            getBusiness().delete(entity.getId());
        }
//        getBusiness().delete(persistentObjects);
        return new VOResponse(true);
    }

    @Override
    public void afterDeleteGrid(GridControl gridControl) {
        gridControl.reloadData();
    }
}
