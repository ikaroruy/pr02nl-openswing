/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pr02nl.openswing.views;

import br.com.pr02nl.business.MyDelegateCrud;
import java.util.List;
import org.openswing.swing.items.client.ItemsDataLocator;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOListResponse;

/**
 *
 * @author Paulo
 */
public abstract class ItemsGenericoView extends ItemsDataLocator {

    public abstract MyDelegateCrud getBusiness();

    @Override
    public Response loadData(Class valueObjectType) {
        List findAll = getBusiness().findAll();
        return new VOListResponse(findAll, false, findAll.size());
    }
}
