/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pr02nl.openswing.views;

import br.com.pr02nl.business.MyDelegateCrud;
import br.com.pr02nl.model.BaseModel;
import br.com.pr02nl.openswing.config.ViewConfig;
import br.gov.frameworkdemoiselle.util.Beans;
import br.gov.frameworkdemoiselle.util.Reflections;
import org.openswing.swing.lookup.client.LookupController;

/**
 *
 * @author pr02nl
 * @param <T>
 * @param <I>
 */
public class GenericLookupController<T extends BaseModel, I extends MyDelegateCrud> extends LookupController {

    private Class<T> modelClass;
    private Class<I> businessClass;

    protected Class<T> getModelClass() {
        if (modelClass == null) {
            modelClass = Reflections.getGenericTypeArgument(this.getClass(), 0);
        }
        return modelClass;
    }

    protected Class<I> getBusinessClass() {
        if (businessClass == null) {
            businessClass = Reflections.getGenericTypeArgument(this.getClass(), 1);
        }
        return businessClass;
    }

    public void init() {
        setAnchorLastColumn(true);
        setFramePreferedSize(new java.awt.Dimension(400, 600));
        setAllColumnVisible(false);
        setLookupValueObjectClassName(getModelClass().getName());
        setLookupDataLocator(new LookupGenericoView() {
            private MyDelegateCrud delegateCrud;

            @Override
            public MyDelegateCrud getBusiness() {
                if (delegateCrud == null) {
                    delegateCrud = Beans.getReference(getBusinessClass());
                }
                return delegateCrud;
            }

            @Override
            public ViewConfig getConfig() {
                return Beans.getReference(ViewConfig.class);
            }
        });
    }

    @Override
    public void setVisibleColumn(String lookupAttributeName, boolean visible) {
        super.setVisibleColumn(lookupAttributeName, visible);
        setFilterableColumn(lookupAttributeName, visible);
        setSortableColumn(lookupAttributeName, visible);
    }
}
