/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pr02nl.openswing.views;

import br.com.pr02nl.business.MyDelegateCrud;
import br.com.pr02nl.openswing.config.ViewConfig;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;
import org.openswing.swing.lookup.client.LookupDataLocator;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOListResponse;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.message.send.java.GridParams;
import org.openswing.swing.tree.java.OpenSwingTreeNode;

/**
 *
 * @author Paulo
 */
public abstract class LookupGenericoView extends LookupDataLocator {

    public abstract MyDelegateCrud getBusiness();

    public abstract ViewConfig getConfig();

    @Override
    public Response validateCode(String code) {
        return null;
    }

    @Override
    public Response loadData(int action, int startIndex, Map filteredColumns, ArrayList currentSortedColumns, ArrayList currentSortedVersusColumns, Class valueObjectType) {
        int count = getBusiness().count(filteredColumns, null);
        if (action == GridParams.PREVIOUS_BLOCK_ACTION) {
            startIndex = Math.max(startIndex - getConfig().getViewBlockSize(), 0);
        } else if (action == GridParams.LAST_BLOCK_ACTION) {
            startIndex = Math.max(count - getConfig().getViewBlockSize(), 0);
        }
        List findAll = getBusiness().findAll(filteredColumns, null,
                new int[]{startIndex, getConfig().getViewBlockSize()}, currentSortedColumns,
                currentSortedVersusColumns);
        VOListResponse voListResponse = new VOListResponse(findAll, count > startIndex + getConfig().getViewBlockSize(), findAll.size());
        voListResponse.setTotalAmountOfRows(count);
        return voListResponse;
    }

    @Override
    public Response getTreeModel(JTree tree) {
        return new VOResponse(new DefaultTreeModel(new OpenSwingTreeNode()));
    }
}
