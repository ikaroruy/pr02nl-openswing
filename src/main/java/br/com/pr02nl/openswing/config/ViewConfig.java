/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pr02nl.openswing.config;

/**
 *
 * @author Paulo Roberto
 */
//@Configuration(resource = "config")
public interface ViewConfig {

//    private int viewBlockSize;
//    private String fileLocalization;
//    private String servidor;
//    private String server;
//    private String servidorSDAi;
//    private String logoCoelce;
//    private String pathApp;
//    private String pathRest;
    public int getViewBlockSize();

    public String getFileLocalization();

    public String getServidor();

    public String getServidorSDAi();

    public String getLogoCoelce();

    public String getServer();

    public String getPathApp();

    public String getPathRest();
}
